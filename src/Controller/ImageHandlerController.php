<?php

namespace App\Controller;

use App\Entity\Folder;
use App\Entity\Image;
use App\Form\FolderSortType;
use App\Form\ImageRenameForm;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ImageHandlerController extends Controller
{

    /**
     * @Route("/view/edit-form/{id}", name="view_edit_form", requirements={"id"="\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Image $image
     * @return \Symfony\Component\HttpFoundation\Response
     * @ParamConverter("image", class="App\Entity\Image")
     */
    public function viewFormEdit(Image $image)
    {
        $form = $this->createForm(ImageRenameForm::class, null);
        return $this->render('view_edit_form.html.twig', [
            'form' => $form->createView(),
            'image' => $image,
        ]);
    }

    /**
     * @Route("/view/sort-folder-form/{id}", name="view_sort_folder_form", requirements={"id"="\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @param Folder $folder
     * @return \Symfony\Component\HttpFoundation\Response
     * @ParamConverter("folder", class="App\Entity\Folder")
     */
    public function viewFormSortFolder(Folder $folder)
    {
        $form = $this->createForm(FolderSortType::class, null);
        return $this->render('view_sort_folder_form.html.twig', [
            'form' => $form->createView(),
            'folder' => $folder,
        ]);
    }

    /**
     * @Route("/edit-image/{id}", name="edit_image", requirements={"id"="\d+"})
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param Image $image
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     * @ParamConverter("image", class="App\Entity\Image")
     */
    public function RenameImageAction(Request $request, Image $image, ObjectManager $manager)
    {
        $data = $request->request->get('image_rename_form');
        $name = trim($name = $data['name']) ? trim($name = $data['name']) : null;
        $position = trim($data['position']) ? trim($data['position']) : null;
        $image->setName($name);
        $image->setPosition($position);
        $manager->persist($image);
        $manager->flush();
        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @Route("/edit-folder/{id}", name="edit_folder", requirements={"id"="\d+"})
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param Folder $folder
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     * @ParamConverter("folder", class="App\Entity\Folder")
     */
    public function EditFolderAction(Request $request, Folder $folder, ObjectManager $manager)
    {
        $data = $request->request->get('folder_sort');
        $pos_val = trim($data['position']);
        if ($pos_val == '') {
            $folder->setPosition(null);
        } else {
            $folder->setPosition($pos_val);
        }
        $manager->persist($folder);
        $manager->flush();
        return $this->redirectToRoute('index');
    }

    public function recentPagePogination($currentPage)
    {
        $repository = $this->getDoctrine()->getRepository(Folder::class);
        $pages = ceil(count($repository->findAll()) / 12);
        return $this->render('pages_pogination.html.twig', [
            'pages' => $pages,
            'currentPage' => $currentPage,
        ]);
    }
}