<?php


namespace App\Controller;

use App\Entity\Folder;
use App\Entity\Image;
use App\Form\FolderType;
use App\Repository\FolderRepository;
use App\Repository\ImageRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends Controller
{
    /**
     * @Route("/", name="index")
     * @param FolderRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(FolderRepository $repository)
    {
        $folders = $repository->sortByPositionAndCreatedDate();
        return $this->render('index.html.twig', [
            'folders' => $folders,
            'currentPage' => 1,
        ]);
    }

    /**
     * @Route("/page/{page}", name="page", requirements={"page"="\d+"})
     * @param FolderRepository $repository
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAlbumsAction(FolderRepository $repository, $page = 1)
    {
        $page = ($page == 0) ? 1 : $page;
        $folders = $repository->sortByPositionAndCreatedDate($page);
        if (!$folders){
            return $this->redirectToRoute('index');
        }
        return $this->render('index.html.twig', [
            'folders' => $folders,
            'currentPage' => $page,
        ]);
    }

    /**
     * @Route("/create-folder", name="create_folder")
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createFolderAction(Request $request, ObjectManager $manager)
    {
        $folder = new Folder();
        $form = $this->createForm(FolderType::class, $folder);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $manager->persist($formData);
            $manager->flush();
            $this->addFlash('success', 'Альбом успешно создан!');
            return $this->redirectToRoute('show_photos', [
                'id' => $formData->getId(),
            ]);
        }
        return $this->render('create_folder.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/show_photos/{id}", name="show_photos", requirements={"id"="\d+"})
     * @param FolderRepository $repository
     * @param $id
     * @param ImageRepository $imageRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(FolderRepository $repository, $id, ImageRepository $imageRepository)
    {
        $folder = $repository->find($id);
        $images = $imageRepository->sortByPosition($folder->getId());
        return $this->render('show_photos.html.twig', [
            'folder' => $folder,
            'images' => $images,
        ]);
    }

    /**
     * @Route("/upload-files/{id}", name="upload_files", requirements={"id"="\d+"})
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param Folder $folder
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     * @ParamConverter("folder", class="App\Entity\Folder")
     */
    public function uploadFilesAction(Request $request, Folder $folder, ObjectManager $manager)
    {
        if ($request->getMethod() == 'POST'){
            /** @var UploadedFile $file */
            foreach ($request->files as $file){
                $image = new Image();
                $image->setFolder($folder);
                $fileName = md5(uniqid()).'.'.$file->guessExtension();

                try {
                    $file->move($this->getParameter('images_directory'), $fileName);
                } catch (FileException $e) {
                    return new JsonResponse(["success" => false], 500);
                }
                $image->setImage($fileName);
                $manager->persist($image);
            }
            $manager->flush();
            return new JsonResponse(["success" => true], 200);
        }
        return $this->redirectToRoute('index');
    }
}