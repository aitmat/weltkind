<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setEmail('Admin@mail.ru')
            ->setUsername('Admin')
            ->setEnabled(1)
            ->setPlainPassword('admin')
            ->setRoles(['ROLE_ADMIN'])
            ;
        $manager->persist($user);

        $user = new User();
        $user
            ->setEmail('User@mail.ru')
            ->setUsername('User')
            ->setEnabled(1)
            ->setPlainPassword('user')
        ;
        $manager->persist($user);

        $manager->flush();
    }
}