<?php

namespace App\DataFixtures;

use App\Entity\Folder;
use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\DependencyInjection\ContainerInterface;


class FolderFixtures extends Fixture
{
    public $folders = [];
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('ru_RU');


        for ($i = 0; $i < 35; $i++) {
            $folder = new Folder();
            $folder->setName($faker->realText(50));
            $folder->setPosition($i);
            $folder->setCreatedDate($faker->dateTimeBetween('-3 years', 'now'));
            $folder->setDescription($faker->realText(500));
            $manager->persist($folder);
            $this->folders[] = $folder;
        }

        for ($j = 0; $j < count($this->folders); $j++){
            for ($k = 0; $k < rand(4, 17); $k++){
                $image = new Image();
                $image->setName($faker->realText(50));
                $image->setPosition($k);
                $imageName = $faker->image($this->container->getParameter('images_directory'));
                $explode = explode('/', $imageName);
                $image->setImage($explode[count($explode) -1]);
                $image->setFolder($this->folders[$j]);
                $manager->persist($image);
            }
        }

        $manager->flush();
    }
}