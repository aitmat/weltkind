<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\FolderRepository")
 * @ORM\Table(name="Folder")
 */
class Folder
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Название альбома обязательно для заполнения")
     * @var string
     * @ORM\Column(type="string", nullable=false)
     *
     */
    private $name;

    /**
     *
     * @Assert\DateTime(message="Выберите корректную дату")
     * @Assert\NotBlank(message="Поле обязательно для заполнения")
     * @var \DateTime $date
     * @ORM\Column(type="datetime", nullable=false)
     *
     */
    private $createdDate;


    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="folder")
     */
    private $images;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return Folder
     */
    public function setName(string $name): Folder
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName():? string
    {
        return $this->name;
    }

    /**
     * @param \DateTime $createdDate
     * @return Folder
     */
    public function setCreatedDate(\DateTime $createdDate): Folder
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate():? \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param string $description
     * @return Folder
     */
    public function setDescription(string $description): Folder
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription():? string
    {
        return $this->description;
    }

    /**
     * @param mixed $images
     * @return Folder
     */
    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $position
     * @return Folder
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

}