<?php
namespace App\Form;

use App\Entity\Folder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FolderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Тема',
                'required' => true,
            ])
            ->add('createdDate', DateTimeType::class, [
                'label' => 'Дата',
                'widget' => 'single_text',
                'required' => true,
                'by_reference' => true,
                'attr' => ['class' => 'js-datepicker'],
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Текст',
                'required' => false,
                'attr' => ['class' => 'article-text-area'],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Создать',
                'attr' => ['class' => 'btn btn-primary']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Folder::class,
            'attr' => ['id' => 'myForm'],
            'required' => true,
            'validation_groups'
        ]);
    }
}