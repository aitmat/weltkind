<?php

namespace App\Repository;

use App\Entity\Folder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;



/**
 * @method Folder|null find($id, $lockMode = null, $lockVersion = null)
 * @method Folder|null findOneBy(array $criteria, array $orderBy = null)
 * @method Folder[]    findAll()
 * @method Folder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FolderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Folder::class);
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array|\Doctrine\ORM\Query
     */
    public function sortByPositionAndCreatedDate($page = 1, $limit = 12)
    {
        $startPoint = ($limit * $page) - $limit;
        $em = $this->getEntityManager();
        $sql = "SELECT * FROM weltkind.Folder f ORDER BY  f.position is null ASC, f.position ASC LIMIT {$startPoint}, {$limit}";
        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata('App\Entity\Folder', 'f');
        $query = $em->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }
}