<?php

namespace App\Repository;

use App\Entity\Image;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Image|null find($id, $lockMode = null, $lockVersion = null)
 * @method Image|null findOneBy(array $criteria, array $orderBy = null)
 * @method Image[]    findAll()
 * @method Image[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Image::class);
    }

    /**
     * @param $id_folder
     * @return Image[] Returns an array of Image objects
     */
    public function sortByPosition($id_folder)
    {
        $em = $this->getEntityManager();
        $sql = "SELECT * FROM weltkind.Image i WHERE i.folder_id = {$id_folder} ORDER BY i.position is null ASC, i.position ASC, i.id DESC";
        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata('App\Entity\Image', 'i');
        $query = $em->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }
}